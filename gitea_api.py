# https://try.gitea.io/api/swagger

from typing import Optional
import requests
import aiohttp
import asyncio
import os
from urllib.parse import urlparse
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from utils import (delete_folder, unzip_file, get_folder_size,
                   bytes_to_megabytes)


class GithubApiError(Exception):
    """
    ### Custom class for working with errors
    """


class GiteaApi:

    def __init__(self,
                 url: str = None,
                 token: Optional[str] = None,
                 headers: Optional[dict] = None):

        self.url = url + "/api/v1/"
        self.token = token
        self.headers = headers
        self.get_default_headers()
        if self.token:
            self.headers['Authorization'] = f'token {self.token}'

    def get_default_headers(self):
        if self.headers is None:
            headers = requests.utils.default_headers()
            headers.update({
                "User-Agent": "My User Agent 1.0",
            })
            self.headers = headers

    def get_domain_name(self, url:str):
        parsed_url = urlparse(url)
        domain = parsed_url.netloc.split('.')
        if len(domain) > 1:
            return '_'.join(domain[:-1])
        else:
            return domain[0]

    def get_response(self, tail:str):
        #max-retries-exceeded-with-url-in-requests
        session = requests.Session()
        retry = Retry(connect=3, backoff_factor=0.5)
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        
        response = session.get(f"{self.url}{tail}", headers=self.headers,timeout=10)

        if response.status_code == 200:
            return response.json()
        else:
            print(f"Произошла ошибка: {response.status_code}")

    def check_user_exist(self, user: str) -> bool:
        response = requests.get(f"{self.url}/users/{user}",
                                headers=self.headers,
                                timeout=10)

        if response.status_code == 200:
            return True
        else:
            return False

    def get_all_repos(self, repo_exceptions:list = []) -> list[dict]:

        repositories = self.get_response("/user/repos")

        if repo_exceptions:
            repositories = [repo for repo in repositories if repo.get('name') not in repo_exceptions]
        return repositories

    def get_all_repos_orgs(self, user: str, org_exceptions:list = [], repo_exceptions:list = []) -> list[dict]:
        """
        Method for getting all repositories, through analysis of available organizations,
        shows more available repositories than "/user/repos"
        """

        orgs = []
        repositories = []

        orgs = orgs + self.get_response(f"/users/{user}/orgs")
        orgs = orgs + self.get_response("/orgs")

        if org_exceptions:
            orgs = [org for org in orgs if org.get('name') not in org_exceptions]

        unique_names = set()
        unique_orgs = [d for d in orgs if d['name'] not in unique_names and (unique_names.add(d['name']) or True)]
            
        for org in unique_orgs:
            repositories = repositories + self.get_response(f"/orgs/{org.get('name')}/repos")

        if repo_exceptions:
            repositories = [repo for repo in repositories if repo.get('name') not in repo_exceptions]

        return repositories

    async def download_file(self, session: aiohttp.client.ClientSession,
                            url: str, path_to_save: str) -> Optional[str]:

        try:
            async with session.get(url, headers=self.headers) as response:
                if response.status == 200:
                    with open(path_to_save, 'wb') as file:
                        file.write(await response.read())
                        print(f'Download file: {path_to_save}')
                        print(f'URL: {url}')

                        return path_to_save
                else:
                    print(f'URL: {url}')
                    print(f'Failed response: {response.status}')

        except Exception as e:
            print(f'URL: {url}')
            print(f'Failed to download: {e}')

    async def create_tasks_download_files(self, urls: list[dict]):
        '''
        urls = [{"url": url, "path_to_save": path_to_save}]
        '''
        async with aiohttp.ClientSession() as session:
            tasks = [
                self.download_file(
                    session, url.get('url'),
                    os.path.join(url.get('folder_to_save'),
                                 url.get('name_repo') + '.zip'))
                for url in urls
            ]
            await asyncio.gather(*tasks)

    def save_repos_zip(
        self,
        urls: list[dict],
        update_repo: bool = False,
    ):

        result = asyncio.run(self.create_tasks_download_files(urls))

        for url in urls:

            zip_file_path = os.path.join(url.get('folder_to_save'),
                                         url.get('name_repo') + '.zip')
            extract_folder = os.path.join(url.get('folder_to_save'),
                                          url.get('name_repo'))

            if update_repo and os.path.exists(extract_folder):
                delete_folder(extract_folder)

            unzip_file(zip_file_path, extract_folder)
            if os.path.exists(extract_folder):
                size_folder = round(
                    bytes_to_megabytes(get_folder_size(extract_folder)), 2)
                print(f"Folder size: {size_folder} Mb\n")

        return result
    
    def save_all_repos(self, base_folder: str, repo_exceptions: list = [], update_repo: bool = False):
        repos = []
        urls = []

        if not os.path.exists(base_folder):
            print(f'{base_folder} not exists')
            return

        repos = self.get_all_repos()

        folder_to_save = os.path.join(base_folder, self.get_domain_name(self.url))

        if not os.path.exists(folder_to_save):
            os.makedirs(folder_to_save)

        for repo in repos:
            if not repo.get('name') in repo_exceptions:

                url_zip = f"{self.url}repos/{repo.get('full_name')}/archive/{repo.get('default_branch')}.zip"
                name_repo = repo.get('name')

                urls.append({
                    "url": url_zip,
                    "folder_to_save": folder_to_save,
                    "name_repo": name_repo,
                })

        self.save_repos_zip(urls, update_repo=update_repo)