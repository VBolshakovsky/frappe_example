# https://docs.github.com/ru/rest

from typing import Optional
import requests
import aiohttp
import asyncio
import os

from utils import (delete_folder, unzip_file, get_folder_size,
                   bytes_to_megabytes)


class GithubApiError(Exception):
    """
    ### Custom class for working with errors
    """


class GithubApi:

    def __init__(self,
                 token: Optional[str] = None,
                 headers: Optional[dict] = None):

        self.token = token
        self.headers = headers
        self.get_default_headers()
        if self.token:
            self.headers['Authorization'] = f'token {self.token}'

    def get_default_headers(self):
        if self.headers is None:
            headers = requests.utils.default_headers()
            headers.update({
                'User-Agent': 'My User Agent 1.0',
            })
            self.headers = headers

    def check_user_exist(self, name: str) -> bool:
        response = requests.get(f'https://api.github.com/users/{name}',
                                headers=self.headers,
                                timeout=10)

        if response.status_code == 200:
            return True
        else:
            return False

    def get_repo_list(self, name: str) -> list[dict]:
        per_page = 100
        page = 1
        repositories = []

        while True:
            params = {'per_page': per_page, 'page': page}

            response = requests.get(
                f'https://api.github.com/users/{name}/repos',
                params=params,
                headers=self.headers,
                timeout=10)

            if response.status_code == 200:
                page_repos = response.json()
                if len(page_repos) == 0:
                    break
                else:
                    repositories.extend(page_repos)
                    page += 1
            else:
                print(f'Failed response get_repo_list: {response.status}')
                break

        return repositories

    async def download_file(self, session: aiohttp.client.ClientSession,
                            url: str, path_to_save: str) -> Optional[str]:

        try:
            async with session.get(url, headers=self.headers) as response:
                if response.status == 200:
                    with open(path_to_save, 'wb') as file:
                        file.write(await response.read())
                        print(f'Download file: {path_to_save}')
                        print(f'URL: {url}')

                        return path_to_save
                else:
                    print(f'URL: {url}')
                    print(f'Failed response: {response.status}')

        except Exception as e:
            print(f'URL: {url}')
            print(f'Failed to download: {e}')

    async def create_tasks_download_files(self, urls: list[dict]):
        '''
        urls = [{"url": url, "path_to_save": path_to_save}]
        '''
        async with aiohttp.ClientSession() as session:
            tasks = [
                self.download_file(
                    session, url.get('url'),
                    os.path.join(url.get('folder_to_save'),
                                 url.get('name_repo') + '.zip'))
                for url in urls
            ]
            await asyncio.gather(*tasks)

    def save_repos_zip(
        self,
        urls: list[dict],
        update_repo: bool = False,
    ):

        result = asyncio.run(self.create_tasks_download_files(urls))

        for url in urls:

            zip_file_path = os.path.join(url.get('folder_to_save'),
                                         url.get('name_repo') + '.zip')
            extract_folder = os.path.join(url.get('folder_to_save'),
                                          url.get('name_repo'))

            if update_repo and os.path.exists(extract_folder):
                delete_folder(extract_folder)

            unzip_file(zip_file_path, extract_folder)
            if os.path.exists(extract_folder):
                size_folder = round(
                    bytes_to_megabytes(get_folder_size(extract_folder)), 2)
                print(f"Folder size: {size_folder} Mb\n")

        return result

    def save_repos_from_name(self,
                             name: str,
                             base_folder: str,
                             update_repo: bool = False,
                             repo_exceptions: list = []) -> None:
        repos = []
        urls = []

        if not os.path.exists(base_folder):
            print(f'{base_folder} not exists')
            return

        if not self.check_user_exist(name):
            print(f'{name} not found')
            return

        repos = self.get_repo_list(name)

        if not repos:
            print(f'{name} - repositories not found')
            return

        folder_to_save = os.path.join(base_folder, name)

        if not os.path.exists(folder_to_save):
            os.makedirs(folder_to_save)

        for repo in repos:
            if not repo.get('name') in repo_exceptions:
                url_zip = f"{repo.get('html_url')}/archive/refs/heads/{repo.get('default_branch')}.zip"
                name_repo = repo.get('name')

                urls.append({
                    "url": url_zip,
                    "folder_to_save": folder_to_save,
                    "name_repo": name_repo,
                })

        self.save_repos_zip(urls, update_repo=update_repo)
