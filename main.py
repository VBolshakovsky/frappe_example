import os
from github_api import GithubApi
from gitea_api import GiteaApi
from urls.github_urls import get_other_github_urls
from dotenv import load_dotenv

BASE_FOLDER = "git"
NAME_USERS_GITHUB = [
    "kid1194", "AgileShift", "yrestom", "aakvatech", "alphabit-technology",
    "finbyz", "datahenge", "the-commit-company", "aisenyi",
    "midocean-technologies", "aerele", "8848digital", "shridarpatil",
    "dokos-io", "libracore"
]
REPO_EXCEPTIONS = [
    "frappe", "erpnext", "data-gov-in", "erpnext_documentation", "fonts",
    "frappe_docs"
]


def save_other_github_repos(base_folder: str, update_repo: bool = True):
    """
    Saves custom repositories from GitHub

    Args:
    - base_folder: The base folder where the repositories will be saved.
    - update_repo: Delete the current repo folder, before unpacking
    """
    gh = GithubApi()

    folder_to_save = os.path.join(base_folder, "other")

    if not os.path.exists(folder_to_save):
        os.makedirs(folder_to_save)

    urls = get_other_github_urls(folder_to_save)

    gh.save_repos_zip(urls, update_repo=update_repo)


def save_github_repos_for_name(
    names: list,
    base_folder: str,
    repo_exceptions: list = [],
    update_repo: bool = True,
):
    """
    Saves GitHub repositories for the user names

    Args:
        names (list): A list of names for which repositories will be saved
        base_folder (str): The base folder where the repositories will be saved
        repo_exceptions (list, optional): A list of repository names to exclude from saving
        update_repo (bool, optional): Delete the current repo folder, before unpacking
    """
    gh = GithubApi()
    for name in names:
        gh.save_repos_from_name(name,
                                base_folder,
                                update_repo=update_repo,
                                repo_exceptions=repo_exceptions)


def save_all_gitea_repos(
    base_folder: str,
    repo_exceptions: list = [],
    update_repo: bool = True,
):

    load_dotenv()
    gitea_url = os.getenv("gitea_url")
    gitea_user = os.getenv("gitea_user")
    gitea_token = os.getenv("gitea_token")

    gitea = GiteaApi(url=gitea_url, token=gitea_token)
    # Downloading all repositories available to the user from gitea
    gitea.save_all_repos(base_folder,
                         repo_exceptions=repo_exceptions,
                         update_repo=update_repo)

    # repos = gitea.get_all_repos_orgs(user=gitea_user,
    #                                  org_exceptions=["frappe", "redmine"],
    #                                  repo_exceptions=[
    #                                      "camunda_viewer", "yarn",
    #                                      "ketu-ui-platform", "ketu-platform"
    #                                  ])
    # print(len(repos))
    # for repo in repos:
    #     print(repo.get('name'))


# Loading repositories specified in NAME_USERS_GITHUB
save_github_repos_for_name(NAME_USERS_GITHUB,
                           BASE_FOLDER,
                           repo_exceptions=REPO_EXCEPTIONS,
                           update_repo=True)

# Loading repositories from specified URLs specified in urls/github_urls
save_other_github_repos(BASE_FOLDER, update_repo=True)

# Downloading all repositories available to the user from gitea
save_all_gitea_repos(BASE_FOLDER,
                     repo_exceptions=[
                         "camunda_viewer", "yarn", "ketu-ui-platform",
                         "ketu-platform"
                     ],
                     update_repo=True)
