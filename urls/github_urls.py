from utils import get_repo_name_from_url_zip

OTHER_GITHUB_URLS = [
    "https://github.com/YefriTavarez/bench_manager/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/dgii/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/IT_Services/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/fairweather/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/erpnext-spp/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/finance_manager/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/CSF_TZ/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/fimax/archive/refs/heads/master.zip",
    "https://github.com/YefriTavarez/PropMS/archive/refs/heads/master.zip",
    "https://github.com/bailabs/tailpos/archive/refs/heads/master.zip",
    "https://github.com/Monogramm/erpnext_ocr/archive/refs/heads/master.zip",
    "https://github.com/mymi14s/frappe_system_monitor/archive/refs/heads/master.zip",
    "https://github.com/TridotsTech/go1cms/archive/refs/heads/master.zip",
    "https://github.com/gavindsouza/photos/archive/refs/heads/main.zip",
    "https://github.com/gavindsouza/frappe-active-users/archive/refs/heads/main.zip",
    "https://github.com/gavindsouza/frappe_server_performance/archive/refs/heads/develop.zip",
    "https://github.com/pibico/pibicut/archive/refs/heads/develop.zip",
    "https://github.com/pibico/pibiDAV/archive/refs/heads/develop.zip",
    "https://github.com/alyf-de/banking/archive/refs/heads/version-14.zip",
    "https://github.com/resilient-tech/bank_integration/archive/refs/heads/version-14.zip",
    "https://github.com/alyf-de/landa/archive/refs/heads/version-13-hotfix.zip",
]


def get_other_github_urls(folder_to_save: str) -> list[dict]:
    """
    Create GitHub urls
    """
    urls = []
    for url in OTHER_GITHUB_URLS:
        urls.append({
            "url": url,
            "folder_to_save": folder_to_save,
            "name_repo": get_repo_name_from_url_zip(url),
        })

    return urls
