import zipfile
import shutil
import os
import re


def delete_folder(path):

    def onerror(func, path, exc_info):
        """
        Error handler for ``shutil.rmtree``.

        If the error is due to an access error (read only file)
        it attempts to add write permission and then retries.

        If the error is for another reason it re-raises the error.
        
        Usage : ``shutil.rmtree(path, onerror=onerror)``
        """
        import stat
        # Is the error an access error?
        if not os.access(path, os.W_OK):
            os.chmod(path, stat.S_IWUSR)
            func(path)
        else:
            raise

    try:
        if os.path.exists(path):
            shutil.rmtree(path, onerror=onerror)
            print(f"folder delete: {path}")

    except OSError as e:
        print(f"Error on delete {path}: {e}")


def unzip_file(zip_file_path, extract_folder, remove_zip=True):
    try:
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(extract_folder)
            print(f"Unzip file: {os.path.basename(zip_file_path)}")
            print(f"Path: {extract_folder}")
        if remove_zip:
            os.remove(zip_file_path)
            print(f"File zip delete: {os.path.basename(zip_file_path)}")
    except FileNotFoundError:
        print(f'Ошибка: Файл не найден: {zip_file_path}')
    except zipfile.BadZipFile:
        print(f'Ошибка: Неверный формат ZIP архива: {zip_file_path}')
    except Exception as e:
        print(f'Произошла ошибка при обработке архива: {e}')


def get_folder_size(folder_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(folder_path):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            total_size += os.path.getsize(file_path)
    return total_size


def bytes_to_megabytes(bytes):
    megabytes = bytes / (1024 * 1024)
    return megabytes


def get_repo_name_from_url_zip(url_zip: str) -> str:
    repo_name = re.search(r'.+/(.+?)/archive', url_zip).group(1)
    return repo_name
